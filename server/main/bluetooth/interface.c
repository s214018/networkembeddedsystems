
#include "interface.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_log.h"

#include "esp_bt.h"
#include "esp_gap_ble_api.h"
#include "esp_gatts_api.h"
#include "esp_bt_defs.h"
#include "esp_bt_main.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAG "ble interface"

// Main application function
esp_err_t ble_interface_init(void)
{
    esp_err_t ret;

    // Release the memory of classic BT controller as we're not using it
    ESP_ERROR_CHECK(esp_bt_controller_mem_release(ESP_BT_MODE_CLASSIC_BT));

    // Initialize the BT controller with the default config
    esp_bt_controller_config_t bt_cfg = BT_CONTROLLER_INIT_CONFIG_DEFAULT();
    ret = esp_bt_controller_init(&bt_cfg);
    if (ret) {
        ESP_LOGE(TAG, "%s enable controller failed: %s", __func__, esp_err_to_name(ret));
        return ret;
    }

    // Enable the BT controller in BLE mode
    ret = esp_bt_controller_enable(ESP_BT_MODE_BLE);
    if (ret) {
        ESP_LOGE(TAG, "%s enable controller failed: %s", __func__, esp_err_to_name(ret));
        return ret;
    }

    // Initialize the Bluedroid library
    ret = esp_bluedroid_init();
    if (ret) {
        ESP_LOGE(TAG, "%s init bluetooth failed: %s", __func__, esp_err_to_name(ret));
        return ret;
    }

    // Enable Bluedroid
    ret = esp_bluedroid_enable();
    if (ret) {
        ESP_LOGE(TAG, "%s enable bluetooth failed: %s", __func__, esp_err_to_name(ret));
        return ret;
    }

    //ble_gap_init();

	ESP_LOGI(TAG,"Bluetooth Driver Initialized");

	return ret;
}

esp_err_t ble_interface_deinit(void)
{
	esp_err_t err;

    //ble_gap_deinit();

	err = esp_bluedroid_disable();
	if (err)
	{
		ESP_LOGE(TAG,"disable bluedroid failed");
		return err;
	}

	err = esp_bluedroid_deinit();
	if (err)
	{
		ESP_LOGE(TAG,"deinitialize bluedroid failed");
		return err;
	}

	err = esp_bt_controller_disable();
	if (err)
	{
		ESP_LOGE(TAG,"disable bt controller failed");
		return err;
	}

	err = esp_bt_controller_deinit();
	if (err)
	{
		ESP_LOGE(TAG,"deinitialize bt controller failed");
		return err;
	}

	return err;
}