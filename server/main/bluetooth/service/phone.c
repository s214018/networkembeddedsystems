
#include "ble_service.h"

#include "esp_gatts_api.h"
#include "esp_log.h"

#include "../common.h"

#include <string.h>

#define GATTS_TABLE_TAG "ble wifi service"

/* Service */
// UUIDs for the GATT service and characteristics
uint16_t GATTS_SERVICE_UUID_PHONE      = 0x00F0;
uint16_t GATTS_CHAR_UUID_PHONE       = 0x0F01;

uint8_t heart_measurement_ccc[2]      = {0x00, 0x00};
uint8_t char_value[4]                 = {0x48, 0x65, 0x79, 0x6F}; // Val: Heyo
uint16_t phone_handle_table[HRS_IDX_NB];


/* Full Database Description - Used to add attributes into the database */
// Array to hold the attribute database
esp_gatts_attr_db_t phone_db[HRS_IDX_NB] =
{
    // Service Declaration
    [IDX_SVC]        =
    {{ESP_GATT_AUTO_RSP}, {ESP_UUID_LEN_16, (uint8_t *)&primary_service_uuid, ESP_GATT_PERM_READ,
      sizeof(uint16_t), sizeof(GATTS_SERVICE_UUID_PHONE), (uint8_t *)&GATTS_SERVICE_UUID_PHONE}},

    /* Characteristic Declaration */
    [IDX_CHAR_A]     =
    {{ESP_GATT_AUTO_RSP}, {ESP_UUID_LEN_16, (uint8_t *)&character_declaration_uuid, ESP_GATT_PERM_READ,
      CHAR_DECLARATION_SIZE, CHAR_DECLARATION_SIZE, (uint8_t *)&char_prop_read_write_notify}},

    /* Characteristic Value */
    [IDX_CHAR_VAL_A] =
    {{ESP_GATT_AUTO_RSP}, {ESP_UUID_LEN_16, (uint8_t *)&GATTS_CHAR_UUID_PHONE, ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE,
      GATTS_DEMO_CHAR_VAL_LEN_MAX, sizeof(char_value), (uint8_t *)char_value}},

    /* Client Characteristic Configuration Descriptor */
    [IDX_CHAR_CFG_A]  =
    {{ESP_GATT_AUTO_RSP}, {ESP_UUID_LEN_16, (uint8_t *)&character_client_config_uuid, ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE,
      sizeof(uint16_t), sizeof(heart_measurement_ccc), (uint8_t *)heart_measurement_ccc}},
};