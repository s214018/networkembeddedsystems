
#include "ble_service.h"

#include "esp_gatts_api.h"
#include "esp_log.h"

#include "../common.h"

#include <string.h>

#define GATTS_TABLE_TAG "ble wifi service"

/* Service */
// UUIDs for the GATT service and characteristics
uint16_t GATTS_SERVICE_UUID_TEST      = 0x00FF;
uint16_t GATTS_CHAR_UUID_TEST_A       = 0xFF01;

uint8_t heart_measurement_ccc[2]      = {0x00, 0x00};
uint8_t char_value[2]                 = {0x87, 0x00};
uint16_t heart_rate_handle_table[HRS_IDX_NB];


/* Full Database Description - Used to add attributes into the database */
// Array to hold the attribute database
esp_gatts_attr_db_t gatt_db[HRS_IDX_NB] =
{
    // Service Declaration
    [IDX_SVC]        =
    {{ESP_GATT_AUTO_RSP}, {ESP_UUID_LEN_16, (uint8_t *)&primary_service_uuid, ESP_GATT_PERM_READ,
      sizeof(uint16_t), sizeof(GATTS_SERVICE_UUID_TEST), (uint8_t *)&GATTS_SERVICE_UUID_TEST}},

    /* Characteristic Declaration */
    [IDX_CHAR_A]     =
    {{ESP_GATT_AUTO_RSP}, {ESP_UUID_LEN_16, (uint8_t *)&character_declaration_uuid, ESP_GATT_PERM_READ,
      CHAR_DECLARATION_SIZE, CHAR_DECLARATION_SIZE, (uint8_t *)&char_prop_read_write_notify}},

    /* Characteristic Value */
    [IDX_CHAR_VAL_A] =
    {{ESP_GATT_AUTO_RSP}, {ESP_UUID_LEN_16, (uint8_t *)&GATTS_CHAR_UUID_TEST_A, ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE,
      GATTS_DEMO_CHAR_VAL_LEN_MAX, sizeof(char_value), (uint8_t *)char_value}},
};