
#ifndef BLE_SERVICE_H_
#define BLE_SERVICE_H_
#include <stdint.h>

#include "esp_gatt_defs.h"
#include "esp_gatts_api.h"

/* Attributes State Machine */
/*The enumeration elements are set up in the same order as the Heart Rate Profile attributes, starting with the service followed by the characteristics of that service.*/
enum
{
    IDX_SVC, //HRS_IDX_SVC: Heart Rate Service index
    IDX_CHAR_A, //HRS_IDX_HR_MEAS_CHAR: Heart Rate Measurement characteristic index
    IDX_CHAR_VAL_A, //HRS_IDX_HR_MEAS_VAL: Heart Rate Measurement characteristic value index
    HRS_IDX_NB, //HRS_IDX_NB: Number of table elements.
};

extern uint16_t heart_rate_handle_table[HRS_IDX_NB];


/* Service */
// UUIDs for the GATT service and characteristics
extern uint16_t GATTS_SERVICE_UUID_TEST;
extern uint16_t GATTS_CHAR_UUID_TEST_A;

extern esp_gatts_attr_db_t gatt_db[HRS_IDX_NB];

#endif /* WIFI_CONFIG_SERVICE_H_ */
