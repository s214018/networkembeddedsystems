#ifndef COMMON_H
#define COMMON_H

#include "esp_gatt_defs.h"

#define CHAR_DECLARATION_SIZE       (sizeof(uint8_t))
#define GATTS_DEMO_CHAR_VAL_LEN_MAX 500
#define PREPARE_BUF_MAX_SIZE        1024

// UUIDs for the primary service, characteristic declaration, and client configuration
extern const uint16_t primary_service_uuid;
extern const uint16_t character_declaration_uuid;
extern const uint16_t character_client_config_uuid;

extern const uint16_t character_description; //0x2901

extern const uint8_t char_prop_notify;
extern const uint8_t char_prop_read;
extern const uint8_t char_prop_read_notify;
extern const uint8_t char_prop_read_write;
extern const uint8_t char_prop_read_write_notify;
extern const uint8_t char_prop_write;
extern const uint8_t char_prop_write_notify;

#endif // BLE_COMMON_H
