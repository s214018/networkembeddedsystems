#include "gap.h"

#include "esp_log.h"

#include "esp_bt.h"
#include "esp_gap_ble_api.h"
#include "esp_gatts_api.h"
#include "esp_gatt_defs.h"
#include "esp_bt_defs.h"
#include "esp_bt_device.h"
#include "esp_bt_main.h"
#include "esp_gatt_common_api.h"

#define TAG "ble gap"

#define BLE_DEVICE_NAME "FACTORIZE"
#define ADV_INT_MIN					(80) // 100ms  Conversion: (Time (ms) = N * 1.25 ms) Range: 0x0006 (7.5ms) to 0x0C80 (400s)
#define ADV_INT_MAX					(160) // 200ms



/* The max length of characteristic value. When the GATT client performs a write or prepare write operation,
*  the data length must be less than GATTS_DEMO_CHAR_VAL_LEN_MAX.
*/

static uint8_t adv_config_done       = 0;

// Array to hold the raw advertisement data
static uint8_t raw_adv_data[] = {
        0x02, 0x01, 0x06, // Flags: General discoverable and BR/EDR not supported
        0x02, 0x0a, 0xeb, // Tx power level: -21dB
        0x03, 0x03, 0xFF, 0x00, // Complete list of 16-bit Service UUIDs
        0x0f, 0x09, 'E', 'S', 'P', '_', 'G', 'A', 'T', 'T', 'S', '_', 'D','E', 'M', 'O' // Complete local name: ESP_GATTS_DEMO
};

// Array to hold the raw scan response data
static uint8_t raw_scan_rsp_data[] = {
        0x02, 0x01, 0x06, // Flags: General discoverable and BR/EDR not supported
        0x02, 0x0a, 0xeb, // Tx power level: -21dB
        0x03, 0x03, 0xFF,0x00 // Complete list of 16-bit Service UUIDs
};

static uint8_t service_uuid[16] = {
    /* LSB <--------------------------------------------------------------------------------> MSB */
    //first uuid, 16bit, [12],[13] is the value
    0xfb, 0x34, 0x9b, 0x5f, 0x80, 0x00, 0x00, 0x80, 0x00, 0x10, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00,
};

// Struct to hold the advertisement parameters
static esp_ble_adv_params_t adv_params = {
    .adv_int_min         = 0x20, // Minimum advertising interval
    .adv_int_max         = 0x40, // Maximum advertising interval
    .adv_type            = ADV_TYPE_IND, // Advertising type: Connectable undirected advertising (ADV_IND)
    .own_addr_type       = BLE_ADDR_TYPE_PUBLIC, // Own address type: Public device address
    .channel_map         = ADV_CHNL_ALL, // Advertising channel map: All channels used
    .adv_filter_policy   = ADV_FILTER_ALLOW_SCAN_ANY_CON_ANY, // Advertising filter policy: Allow scanning and connections from any device
};

// Function to handle GAP (Generic Access Profile) events
static void gap_event_handler(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param)
{
    switch (event) {
        // Event for completion of setting raw advertisement data
        case ESP_GAP_BLE_ADV_DATA_RAW_SET_COMPLETE_EVT:
            adv_config_done &= (~ADV_CONFIG_FLAG); // Clear the advertisement configuration flag
            if (adv_config_done == 0){ // If all configurations are done
                esp_ble_gap_start_advertising(&adv_params); // Start advertising
            }
            break;
        // Event for completion of setting raw scan response data
        case ESP_GAP_BLE_SCAN_RSP_DATA_RAW_SET_COMPLETE_EVT:
            adv_config_done &= (~SCAN_RSP_CONFIG_FLAG); // Clear the scan response configuration flag
            if (adv_config_done == 0){ // If all configurations are done
                esp_ble_gap_start_advertising(&adv_params); // Start advertising
            }
            break;
        // Event for completion of starting advertising
        case ESP_GAP_BLE_ADV_START_COMPLETE_EVT:
            /* advertising start complete event to indicate advertising start successfully or failed */
            if (param->adv_start_cmpl.status != ESP_BT_STATUS_SUCCESS) {
                ESP_LOGE(TAG, "advertising start failed"); // Log an error message if advertising start failed
            }else{
                ESP_LOGI(TAG, "advertising start successfully"); // Log an info message if advertising started successfully
            }
            break;
        // Event for completion of stopping advertising
        case ESP_GAP_BLE_ADV_STOP_COMPLETE_EVT:
            if (param->adv_stop_cmpl.status != ESP_BT_STATUS_SUCCESS) {
                ESP_LOGE(TAG, "Advertising stop failed"); // Log an error message if advertising stop failed
            }
            else {
                ESP_LOGI(TAG, "Stop adv successfully\n"); // Log an info message if advertising stopped successfully
            }
            break;
        // Event for updating connection parameters
        case ESP_GAP_BLE_UPDATE_CONN_PARAMS_EVT:
            ESP_LOGI(TAG, "update connection params status = %d, min_int = %d, max_int = %d,conn_int = %d,latency = %d, timeout = %d",
                  param->update_conn_params.status,
                  param->update_conn_params.min_int,
                  param->update_conn_params.max_int,
                  param->update_conn_params.conn_int,
                  param->update_conn_params.latency,
                  param->update_conn_params.timeout);
            break;
        default:
            break;
    }
}

void ble_gap_setAdvertisingData(void)
{
    esp_err_t set_dev_name_ret = esp_ble_gap_set_device_name(BLE_DEVICE_NAME);
    if (set_dev_name_ret)
    {
        ESP_LOGE(TAG,"set device name failed, error code = %x", set_dev_name_ret);
    }


    esp_err_t raw_adv_ret = esp_ble_gap_config_adv_data_raw(raw_adv_data, sizeof(raw_adv_data));
    if (raw_adv_ret)
    {
        ESP_LOGE(TAG,"config raw adv data failed, error code = %x ", raw_adv_ret);
    }

    adv_config_done |= ADV_CONFIG_FLAG;

    esp_err_t raw_scan_ret = esp_ble_gap_config_scan_rsp_data_raw(raw_scan_rsp_data, sizeof(raw_scan_rsp_data));
    if (raw_scan_ret)
    {
        ESP_LOGE(TAG,"config raw scan rsp data failed, error code = %x", raw_scan_ret);
    }

    adv_config_done |= SCAN_RSP_CONFIG_FLAG;

}



void ble_gap_startAdvertising(void)
{
	esp_ble_gap_start_advertising(&adv_params);
}



bool ble_gap_init(void)
{
	esp_err_t err = esp_ble_gap_register_callback(gap_event_handler);
	if( err )
	{
		ESP_LOGE(TAG,"Gap register failed with err: %x", err);
		return false;
	}

	return true;
}



void ble_gap_deinit(void)
{
	// Do nothing
}


