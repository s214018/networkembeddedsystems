#include "interface.h"

#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_bt.h"
#include "math.h"

#include <stdbool.h>
#include "esp_gap_ble_api.h"
#include "esp_gatts_api.h"
#include "esp_bt_main.h"
#include "esp_gatt_common_api.h"
#include "esp_timer.h"

#include "common.h"
#include "gap.h"

#include "service/ble_service.h"
#include "service/phone.h"

#include "esp_random.h"

#define TAG "GATT Server"


#define PROFILE_NUM                 1
#define PROFILE_APP_IDX             0
#define ESP_APP_ID                  0x55
#define SAMPLE_DEVICE_NAME          "ESP_GATTS_DEMO"
#define SVC_INST_ID                 0

#define ADV_CONFIG_FLAG             (1 << 0)
#define SCAN_RSP_CONFIG_FLAG        (1 << 1)

static uint16_t gatts_mtu = 23;
static int num_connected = 0;
static uint8_t active_tasking = 0; 
static uint8_t curr_responded = 0;
static uint32_t rand_nr;
static uint16_t curr_answer = 1;

uint64_t start;

//static uint8_t adv_config_done       = 0;

#define NEW_CHAR_UUID 0xFFFF  // Replace with the desired UUID for the new characteristic

// Define an initial value for the new characteristic
static uint8_t init_char_val[] = {0x01, 0x02, 0x03};  // Replace with your desired initial value




// Add a flag to track whether the characteristic has been created
static bool new_char_created = false;

// Function to add a new characteristic
esp_err_t add_new_characteristic(uint16_t service_handle, esp_gatt_if_t gatts_if) {
    esp_err_t ret;

    // Define the UUID and properties for the new characteristic
    esp_bt_uuid_t char_uuid = {
        .len = ESP_UUID_LEN_16,
        .uuid = {
            .uuid16 = NEW_CHAR_UUID, // Define NEW_CHAR_UUID with the desired UUID for the new characteristic
        },
    };

    esp_attr_value_t char_value = {
        .attr_max_len = GATTS_DEMO_CHAR_VAL_LEN_MAX,
        .attr_len = sizeof(init_char_val),
        .attr_value = init_char_val,
    };

    esp_attr_control_t control = {
        .auto_rsp = ESP_GATT_AUTO_RSP,
    };

    esp_gatt_char_prop_t property = ESP_GATT_CHAR_PROP_BIT_WRITE | ESP_GATT_CHAR_PROP_BIT_READ;

    // Add the new characteristic
    ret = esp_ble_gatts_add_char(
        service_handle,
        &char_uuid,
        ESP_GATT_PERM_READ | ESP_GATT_PERM_WRITE,
        property,
        &char_value,
        &control
    );

    if (ret) {
        ESP_LOGE(TAG, "Failed to add new characteristic, error code = %x", ret);
        return ret;
    }

    new_char_created = true;
    ESP_LOGI(TAG, "New characteristic added successfully");

    return ret;
}

typedef struct {
    uint8_t                 *prepare_buf;
    int                     prepare_len;
} prepare_type_env_t;

static prepare_type_env_t prepare_write_env;


struct gatts_profile_inst {
    esp_gatts_cb_t gatts_cb; // GATT server callback function
    uint16_t gatts_if; // GATT server interface
    uint16_t app_id; // Application ID
    uint16_t conn_id; // Connection ID
    uint16_t service_handle; // Service handle
    esp_gatt_srvc_id_t service_id; // Service ID
    uint16_t char_handle; // Characteristic handle
    esp_bt_uuid_t char_uuid; // Characteristic UUID
    esp_gatt_perm_t perm; // Permissions for the characteristic
    esp_gatt_char_prop_t property; // Properties of the characteristic
    uint16_t descr_handle; // Descriptor handle
    esp_bt_uuid_t descr_uuid; // Descriptor UUID
};

// Function prototype for the GATT server profile event handler
static void gatts_profile_event_handler(esp_gatts_cb_event_t event,
					esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param);

// Array to hold the GATT server profile instances. This array will store the GATT server interfaces returned by ESP_GATTS_REG_EVT.
static struct gatts_profile_inst heart_rate_profile_tab[PROFILE_NUM] = {
    [PROFILE_APP_IDX] = {
        .gatts_cb = gatts_profile_event_handler, // GATT server callback function for this profile instance
        .gatts_if = ESP_GATT_IF_NONE,       /* Not get the gatt_if, so initial is ESP_GATT_IF_NONE */
    },
};

void uint32ToBytes(uint32_t value, uint8_t *byteArray) {
    byteArray[0] = (uint8_t)(value & 0xFF);         // Least significant byte (LSB)
    byteArray[1] = (uint8_t)((value >> 8) & 0xFF);
    byteArray[2] = (uint8_t)((value >> 16) & 0xFF);
    byteArray[3] = (uint8_t)((value >> 24) & 0xFF);  // Most significant byte (MSB)
}

uint8_t is_prime_trivial(uint32_t num) {
    uint32_t debug = 0;
    uint8_t ans = 1;
    if (!(num % 2)) {
        ans = 0;
    }
    for (uint32_t i = 3; i < num; i+=2) {
        if (++debug == 1000) {
            ESP_LOGI(TAG, "Still checking primality. Iterated %ld/%ld", i, num);
            debug = 0;
            vTaskDelay(1);
        }
        if (num % i == 0) {
            ans = 0;
        }
        //
    }
    return ans;
}


// Function to handle a prepare write event
void example_prepare_write_event_env(esp_gatt_if_t gatts_if, prepare_type_env_t *prepare_write_env, esp_ble_gatts_cb_param_t *param)
{
    ESP_LOGI(TAG, "prepare write, handle = %d, value len = %d", param->write.handle, param->write.len);
    esp_gatt_status_t status = ESP_GATT_OK;
    if (prepare_write_env->prepare_buf == NULL) {
        // If the prepare buffer is NULL, allocate memory for it
        prepare_write_env->prepare_buf = (uint8_t *)malloc(PREPARE_BUF_MAX_SIZE * sizeof(uint8_t));
        prepare_write_env->prepare_len = 0;
        if (prepare_write_env->prepare_buf == NULL) {
            // If memory allocation failed, log an error and set the status to ESP_GATT_NO_RESOURCES
            ESP_LOGE(TAG, "%s, Gatt_server prep no mem", __func__);
            status = ESP_GATT_NO_RESOURCES;
        }
    } else {
        // If the prepare buffer is not NULL, check if the offset is greater than the maximum size of the buffer
        if(param->write.offset > PREPARE_BUF_MAX_SIZE) {
            status = ESP_GATT_INVALID_OFFSET;
        } else if ((param->write.offset + param->write.len) > PREPARE_BUF_MAX_SIZE) {
            // Check if the sum of the offset and length of the write data is greater than the maximum size of the buffer
            status = ESP_GATT_INVALID_ATTR_LEN;
        }
    }
    /*send response when param->write.need_rsp is true */
    if (param->write.need_rsp){
        // If a response is needed, allocate memory for the response
        esp_gatt_rsp_t *gatt_rsp = (esp_gatt_rsp_t *)malloc(sizeof(esp_gatt_rsp_t));
        if (gatt_rsp != NULL){
            // If memory allocation was successful, set up the response
            gatt_rsp->attr_value.len = param->write.len;
            gatt_rsp->attr_value.handle = param->write.handle;
            gatt_rsp->attr_value.offset = param->write.offset;
            gatt_rsp->attr_value.auth_req = ESP_GATT_AUTH_REQ_NONE;
            memcpy(gatt_rsp->attr_value.value, param->write.value, param->write.len);
            // Send the response
            esp_err_t response_err = esp_ble_gatts_send_response(gatts_if, param->write.conn_id, param->write.trans_id, status, gatt_rsp);
            if (response_err != ESP_OK){
               // If sending the response failed, log an error
               ESP_LOGE(TAG, "Send response error");
            }
            free(gatt_rsp);
        }else{
            // If memory allocation failed, log an error
            ESP_LOGE(TAG, "%s, malloc failed", __func__);
        }
    }
    if (status != ESP_GATT_OK){
        return;
    }
    // Copy the write data to the prepare buffer at the specified offset
    memcpy(prepare_write_env->prepare_buf + param->write.offset,
           param->write.value,
           param->write.len);
    prepare_write_env->prepare_len += param->write.len;

}

// Function to handle an execute write event
void example_exec_write_event_env(prepare_type_env_t *prepare_write_env, esp_ble_gatts_cb_param_t *param){
    if (param->exec_write.exec_write_flag == ESP_GATT_PREP_WRITE_EXEC && prepare_write_env->prepare_buf){
        // If this is an execute write event and there is data in the prepare buffer,
        // log the contents of the prepare buffer
        esp_log_buffer_hex(TAG, prepare_write_env->prepare_buf, prepare_write_env->prepare_len);
    }else{
        // If this is a cancel write event, log a message indicating that
        ESP_LOGI(TAG,"ESP_GATT_PREP_WRITE_CANCEL");
    }
    if (prepare_write_env->prepare_buf) {
        // Free the memory allocated for the prepare buffer and reset it to NULL
        free(prepare_write_env->prepare_buf);
        prepare_write_env->prepare_buf = NULL;
    }
    // Reset the length of the prepare buffer to 0
    prepare_write_env->prepare_len = 0;
}

void start_prime_checking(esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param) {
    uint8_t data[5];
    uint8_t rand_idx = esp_random() % 2;
    uint32_t rand_nr = esp_random() & 0xFFF;
    active_tasking = 1;
    //ESP_LOGI(TAG, "Sending random data. IDX = %d and nr = %lx", rand_idx, rand_nr);
    
    uint32ToBytes(rand_nr, data + 1);

    for (uint8_t i = 1; i < 4; ++i) {
        data[0] = 3+2*(i-1);
        ESP_LOGI(TAG, "Sending random data. IDX = %d and nr = %lx", data[0], rand_nr);
        esp_ble_gatts_send_indicate(gatts_if, i, param->write.handle , sizeof(data), data, false);
    }
}

// Function to handle GATT server profile events
static void gatts_profile_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param)
{
    switch (event) {
        // Event for GATT server registration
        case ESP_GATTS_REG_EVT:{
            ble_gap_setAdvertisingData();
            // Create the attribute table
            esp_err_t create_attr_ret = esp_ble_gatts_create_attr_tab(gatt_db, gatts_if, HRS_IDX_NB, SVC_INST_ID);
            if (create_attr_ret){
                // Log an error if creating the attribute table failed
                ESP_LOGE(TAG, "create attr table failed, error code = %x", create_attr_ret);
            }
        }
       	    break;
        // Event for a read operation on a GATT server
        case ESP_GATTS_READ_EVT:
            // Log a message indicating a read operation occurred
            ESP_LOGI(TAG, "ESP_GATTS_READ_EVT");
       	    break;

        case ESP_GATTS_WRITE_EVT:
            if (!param->write.is_prep){
                // Log the handle and value length of the write event
                ESP_LOGI(TAG, "GATT_WRITE_EVT, handle = %d, value len = %d, value :", param->write.handle, param->write.len);
                esp_log_buffer_hex(TAG, param->write.value, param->write.len);
                uint16_t descr_value = param->write.value[1]<<8 | param->write.value[0];
                ESP_LOGI(TAG, "Descriptor value: %d", descr_value);

                
                if (active_tasking && (descr_value == 0 || descr_value == 1)) {
                    
                    curr_answer = curr_answer && descr_value;
                    ESP_LOGI(TAG, "Curr answer got updated to %d.", curr_answer);
                    if(++curr_responded == num_connected - 1) {
                        ESP_LOGI(TAG, "PRIME CHECKING DONE.");
                        if (curr_answer == 1) {
                            ESP_LOGI(TAG, "%ld IS prime.", rand_nr);
                        } else {
                            ESP_LOGI(TAG, "%ld IS NOT prime.", rand_nr);
                        }
                        active_tasking = 0;
                    }
                    uint64_t end = esp_timer_get_time();
                    ESP_LOGI(TAG, "Distributed algorithm took %llu milliseconds\n", (end - start)/1000);

                }
                if (descr_value == 135 && num_connected > 1) {
                    // start prime checking
                    ESP_LOGI(TAG, "START PRIME CHECKING");
                    //rand_nr = esp_random() & 0xFFF;
                    rand_nr = 100019;
                    start = esp_timer_get_time();
                    is_prime_trivial(rand_nr);
                    uint64_t end = esp_timer_get_time();
                    ESP_LOGI(TAG, "Non-distributed algorithm took %llu milliseconds\n", (end - start)/1000);


                    start = esp_timer_get_time();
                    curr_answer = 1;
                    uint8_t data[5];
                    uint8_t rand_idx = esp_random() % 2;
                    active_tasking = 1;
                    curr_responded = 0;
                    //ESP_LOGI(TAG, "Sending random data. IDX = %d and nr = %lx", rand_idx, rand_nr);
                    
                    uint32ToBytes(rand_nr, data + 1);

                    for (uint8_t i = 1; i < 4; ++i) {
                        data[0] = 3+2*i;
                        ESP_LOGI(TAG, "Sending random data. IDX = %d and nr = %lx", data[0], rand_nr);
                        esp_ble_gatts_send_indicate(gatts_if, i, param->write.handle , sizeof(data), data, false);
                }
                }
                

                // Send a response to the client if needed
                //if (param->write.need_rsp){
                //    esp_ble_gatts_send_response(gatts_if, param->write.conn_id, param->write.trans_id, ESP_GATT_OK, NULL);
                //}
            }else{
                // Handle a prepare write event
                example_prepare_write_event_env(gatts_if, &prepare_write_env, param);
            }
            break;

        case ESP_GATTS_EXEC_WRITE_EVT:
            // Log the execution of a write event
            ESP_LOGI(TAG, "ESP_GATTS_EXEC_WRITE_EVT");
            example_exec_write_event_env(&prepare_write_env, param);
            break;
        case ESP_GATTS_MTU_EVT:
            // Log the Maximum Transmission Unit (MTU) size
            ESP_LOGI(TAG, "ESP_GATTS_MTU_EVT, MTU %d", param->mtu.mtu);
            break;
        case ESP_GATTS_CONF_EVT:
            // Log the confirmation of a write operation
            ESP_LOGI(TAG, "ESP_GATTS_CONF_EVT, status = %d, attr_handle %d", param->conf.status, param->conf.handle);
            break;
        case ESP_GATTS_START_EVT:
            // Log the start of a service
            ESP_LOGI(TAG, "SERVICE_START_EVT, status %d, service_handle %d", param->start.status, param->start.service_handle);
            break;
        case ESP_GATTS_CONNECT_EVT:
            // Log the establishment of a connection
            ESP_LOGI(TAG, "ESP_GATTS_CONNECT_EVT, conn_id = %d", param->connect.conn_id);
            esp_log_buffer_hex(TAG, param->connect.remote_bda, 6);
            esp_ble_conn_update_params_t conn_params = {0};
            num_connected++;
            memcpy(conn_params.bda, param->connect.remote_bda, sizeof(esp_bd_addr_t));
            /* For the iOS system, please refer to Apple official documents about the BLE connection parameters restrictions. */
            conn_params.latency = 0;
            conn_params.max_int = 0x20;    // max_int = 0x20*1.25ms = 40ms
            conn_params.min_int = 0x10;    // min_int = 0x10*1.25ms = 20ms
            conn_params.timeout = 400;     // timeout = 400*10ms = 4000ms
            // Start sending the updated connection parameters to the peer device.
            esp_ble_gap_update_conn_params(&conn_params);
            ESP_LOGI(TAG, "Connection ID: %d", param->connect.conn_id);
            ESP_LOGI(TAG, "number of devices connected: %d", num_connected);
            ble_gap_startAdvertising();
            break;
        case ESP_GATTS_CREAT_ATTR_TAB_EVT:{
            if (param->add_attr_tab.status != ESP_GATT_OK){
                // Log an error if attribute table creation failed
                ESP_LOGE(TAG, "create attribute table failed, error code=0x%x", param->add_attr_tab.status);
            }
            else if (param->add_attr_tab.num_handle != HRS_IDX_NB){
                // Log an error if the number of handles doesn't match HRS_IDX_NB
                ESP_LOGE(TAG, "create attribute table abnormally, num_handle (%d) \
                        doesn't equal to HRS_IDX_NB(%d)", param->add_attr_tab.num_handle, HRS_IDX_NB);
            }
            else {
                // Log a success message if attribute table is created successfully and start the service
                ESP_LOGI(TAG, "create attribute table successfully, the number handle = %d\n",param->add_attr_tab.num_handle);
                memcpy(heart_rate_handle_table, param->add_attr_tab.handles, sizeof(heart_rate_handle_table));
                esp_ble_gatts_start_service(heart_rate_handle_table[IDX_SVC]);
            }
            break;
        }
        // Other events are not handled in this switch-case statement
        case ESP_GATTS_STOP_EVT:
        case ESP_GATTS_OPEN_EVT:
        case ESP_GATTS_CANCEL_OPEN_EVT:
        case ESP_GATTS_CLOSE_EVT:
        case ESP_GATTS_LISTEN_EVT:
        case ESP_GATTS_CONGEST_EVT:
        case ESP_GATTS_UNREG_EVT:
        case ESP_GATTS_DELETE_EVT:
        default:
            break;
    }
}

// This function is a GATT server event handler.
static void gatts_event_handler(esp_gatts_cb_event_t event, esp_gatt_if_t gatts_if, esp_ble_gatts_cb_param_t *param)
{
    // If the event is a register event, store the gatts_if for each profile
    if (event == ESP_GATTS_REG_EVT) {
        // If the registration status is OK, store the gatts_if
        if (param->reg.status == ESP_GATT_OK) {
            heart_rate_profile_tab[PROFILE_APP_IDX].gatts_if = gatts_if;
        } else {
            // If the registration status is not OK, log an error message and return
            ESP_LOGE(TAG, "reg app failed, app_id %04x, status %d",
                    param->reg.app_id,
                    param->reg.status);
            return;
        }
    }
    do {
        int idx;
        // Loop through all profiles
        for (idx = 0; idx < PROFILE_NUM; idx++) {
            // If gatts_if is ESP_GATT_IF_NONE or matches the current profile's gatts_if
            if (gatts_if == ESP_GATT_IF_NONE || gatts_if == heart_rate_profile_tab[idx].gatts_if) {
                // If the current profile has a callback function, call it
                if (heart_rate_profile_tab[idx].gatts_cb) {
                    heart_rate_profile_tab[idx].gatts_cb(event, gatts_if, param);
                }
            }
        }
    } while (0); // End of do-while loop
}

esp_err_t ble_gatt_server_init(void)
{
	esp_err_t err;

	ESP_LOGI(TAG,"Initializing Bluetooth Interface");

	err = esp_ble_gatts_register_callback(gatts_event_handler);
	if(err)
	{
		ESP_LOGE(TAG,"Gatts register failed with err: %x", err);
		return err;
	}

	err = esp_ble_gatts_app_register(ESP_APP_ID);
	if(err)
	{
		ESP_LOGE(TAG,"App register failed with err: %x", err);
		return err;
	}

	err = esp_ble_gatt_set_local_mtu(517);
    if (err)
    {
        ESP_LOGE(TAG,"set local  MTU failed, error code = %x", err);
    }

	ESP_LOGI(TAG,"Bluetooth Interface initialized successfully");

	return err == ESP_OK;
}



esp_err_t ble_gatt_server_deinit(void)
{
	esp_err_t err;
	err = ESP_OK;

	if(esp_bt_controller_get_status() == ESP_BT_CONTROLLER_STATUS_ENABLED)
	{
		//esp_ble_gatts_app_unregister(//gattsif);
	}

	return err == ESP_OK;
}
