#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_bt.h"

#include "esp_gap_ble_api.h"
#include "esp_gatts_api.h"
#include "esp_bt_main.h"
#include "esp_gatt_common_api.h"


#include "main.h"
#include "user_config.h"
#include "bluetooth/common.h"
#include "bluetooth/interface.h"
#include "bluetooth/service/ble_service.h"
#include "bluetooth/gap.h"
#include "bluetooth/gatts_server.h"




// Main application function
void app_main(void)
{
    esp_err_t ret;

    // Initialize Non Volatile Storage
    ret = nvs_flash_init();
    // If there are no free pages or a new version is found, erase and reinitialize the flash
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK( ret );

    ble_interface_init();
    ble_gap_init();
    ble_gatt_server_init();
}