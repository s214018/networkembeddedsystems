/*
 * SPDX-FileCopyrightText: 2021 Espressif Systems (Shanghai) CO LTD
 *
 * SPDX-License-Identifier: Unlicense OR CC0-1.0
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>




/*
HRS_IDX_SVC: Heart Rate Service index
HRS_IDX_HR_MEAS_CHAR: Heart Rate Measurement characteristic index
HRS_IDX_HR_MEAS_VAL: Heart Rate Measurement characteristic value index
HRS_IDX_HR_MEAS_NTF_CFG: Heart Rate Measurement notifications configuration (CCC) index
HRS_IDX_BOBY_SENSOR_LOC_CHAR: Heart Rate Body Sensor Location characteristic index
HRS_IDX_BOBY_SENSOR_LOC_VAL: Heart Rate Body Sensor Location characteristic value index
HRS_IDX_HR_CTNL_PT_CHAR: Heart Rate Control Point characteristic index
HRS_IDX_HR_CTNL_PT_VAL: Heart Rate Control Point characteristic value index
HRS_IDX_NB: Number of table elements.
*/