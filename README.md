## Power Management for ESP32

One way to reduce the power consumption on ESP32 is to force the device to enter a lower-energy consumption mode. ESP32 can enter one of the 5 energy modes:

- Active (All peripherals are active)
- Modem Sleep (WIFI, Bluetooth and Peripherals are disabled)
- Light Sleep (ESP32 code is paused)
- Deep Sleep (ESP32 Core is disabled)
- Hibernation (ULP Coprocessor is disabled)

Here is an example of how `ESP32` can enter a sleep mode:

```c
#include "esp_sleep.h"

void app_main() {
    while (1) {
        // Enter light sleep
        esp_sleep_enable_timer_wakeup(5 * 1000000); // 5 seconds in microseconds
        esp_light_sleep_start();

        // Wait for 5 seconds after waking up
        vTaskDelay(5 * 1000 / portTICK_PERIOD_MS); // 5 seconds in milliseconds
    }
}
```

See [sleep mode comparison](https://diyi0t.com/reduce-the-esp32-power-consumption/) and [sleep modes](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/system/sleep_modes.html). 

### Power Management Locks

> _Power management algorithm included in ESP-IDF can adjust the advanced peripheral bus (APB) frequency, CPU frequency, and put the chip into Light-sleep mode to run an application at smallest possible power consumption, given the requirements of application components_.
>
> _Application components can express their requirements by creating and acquiring power management locks._

Application components can acquire power management locks. These locks allow the applications to keep the CPU at maximum frequency, minimum frequency or enter a light sleep mode if no locks are acquired. 

To allow the application to use the power management locks, [CONFIG_PM_ENABLE](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/kconfig.html#config-pm-enable) can be enabled. The API responsible for power management can be included in the project using:

```c
#include "esp_pm.h"
```

### DFS and Automatic Light-Sleep

Dynamic Frequency Scaling (DFS) and automatic light sleep can be enabled using `esp_pm_configure()`. Its only argument is a structure of `esp_pm_config_t` type, which consists of three properties `max_freq_mhz`, `min_freq_mhz` and `light sleep enable`. Here is an example:

```c
#include "esp_pm.h"

esp_pm_config_t pm_config = {
    .max_freq_mhz = 80,
    .min_freq_mhz = 40,
    .light_sleep_enable = false
};

void app_main(void)
{
    ret = esp_pm_configure(&pm_config);
    ESP_ERROR_CHECK(ret);
}
```
> Automatic Light-sleep is based on FreeRTOS Tickless Idle functionality. If automatic Light-sleep is requested while the option CONFIG_FREERTOS_USE_TICKLESS_IDLE is not enabled in `menuconfig`, `esp_pm_configure()` will return the error `ESP_ERR_NOT_SUPPORTED`.

ESP32 can automatically enter light sleep mode if no peripheral holds the power-lock. See [Power Management](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/system/power_management.html#_CPPv416esp_pm_configurePKv).

## Bluetooth Resources

- [Bluetooth LE](https://picture.iczhiku.com/resource/eetop/SyITTzRUzEfGEmCn.pdf)
- [Intro to GATT](https://www.bluetooth.com/bluetooth-resources/intro-to-bluetooth-gap-gatt/)
- [ESP-IDF Bluetooth GATT Server](https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/bluetooth/esp_gatts.html)
- [Sample BLE project](https://github.com/ashus3868/BLE-Connect)
- [ESP32 BLE Server and Client (Bluetooth Low Energy)](https://randomnerdtutorials.com/esp32-ble-server-client/)
