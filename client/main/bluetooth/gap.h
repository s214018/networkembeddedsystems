#ifndef GAP_H
#define GAP_H

#include "esp_gap_ble_api.h"
#include "esp_gattc_api.h"

void esp_gap_cb(esp_gap_ble_cb_event_t event, esp_ble_gap_cb_param_t *param);
void esp_gattc_cb(esp_gattc_cb_event_t event, esp_gatt_if_t gattc_if, esp_ble_gattc_cb_param_t *param);

#endif // GAP_H