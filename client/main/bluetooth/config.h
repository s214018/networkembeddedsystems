#ifndef CONFIG_H
#define CONFIG_H

#include "esp_gattc_api.h"
#include "esp_gap_ble_api.h"

extern const char remote_device_name[];
extern bool connect;
extern bool get_server;
extern esp_gattc_char_elem_t *char_elem_result;
extern esp_gattc_descr_elem_t *descr_elem_result;

#define GATTC_TAG "GATTC_DEMO"
#define REMOTE_SERVICE_UUID        0x00FF
#define REMOTE_NOTIFY_CHAR_UUID    0xFF01
#define PROFILE_NUM      1
#define PROFILE_A_APP_ID 0
#define INVALID_HANDLE   0

extern esp_bt_uuid_t remote_filter_service_uuid;
extern esp_bt_uuid_t remote_filter_char_uuid;
extern esp_bt_uuid_t notify_descr_uuid;
extern esp_ble_scan_params_t ble_scan_params;

#endif // CONFIG_H
