#ifndef GATTS_H
#define GATTS_H

#include "esp_gattc_api.h"

#define PROFILE_NUM      1

struct gattc_profile_inst {
    esp_gattc_cb_t gattc_cb;
    uint16_t gattc_if;
    uint16_t app_id;
    uint16_t conn_id;
    uint16_t service_start_handle;
    uint16_t service_end_handle;
    uint16_t char_handle;
    esp_bd_addr_t remote_bda;
};

extern struct gattc_profile_inst gl_profile_tab[PROFILE_NUM];
void gattc_profile_event_handler(esp_gattc_cb_event_t event, esp_gatt_if_t gattc_if, esp_ble_gattc_cb_param_t *param);

#endif // GATTS_H
