#ifndef GATT_CLIENT_DEMO_MAIN_H
#define GATT_CLIENT_DEMO_MAIN_H

#include <stdint.h>
#include <string.h>
#include <stdbool.h>
#include "nvs.h"
#include "nvs_flash.h"

#include "esp_bt.h"
#include "esp_gap_ble_api.h"
#include "esp_gattc_api.h"
#include "esp_bt_main.h"
#include "esp_gatt_common_api.h"
#include "esp_log.h"
#include "freertos/FreeRTOS.h"

#include "bluetooth/config.h"
#include "bluetooth/gap.h"

#endif // GATT_CLIENT_DEMO_MAIN_H
